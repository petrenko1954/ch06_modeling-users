#Листинг 6.9: Валидация наличия атрибута name. ЗЕЛЕНЫЙ app/models/user.rb 
#Листинг 6.16: Добавление валидации длины для атрибута name. ЗЕЛЕНЫЙ app/models/user.rb 
#Листинг 6.39: Законченная реализация безопасных паролей. ЗЕЛЕНЫЙ app/models/user.rb 
class User < ActiveRecord::Base
  before_save { self.email = email.downcase }
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }
end

