#Листинг 6.2: Миграция для модели User (создает таблицу users). db/migrate/[timestamp]_create_users.rb 
class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email

      t.timestamps
    end
  end
end
